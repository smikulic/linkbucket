'use strict';

console.log('\'Allo \'Allo! Popup');

chrome.tabs.query({'active': true}, function (tabs) {
  
  // Global
  var selected_index = -1; //Index of the selected list item 
  var storageLinks = localStorage.getItem("storageLinks");//Retrieve the stored data 
  storageLinks = JSON.parse(storageLinks); //Converts string to object 

  if(storageLinks == null) //If there is no data, initialize an empty array 
    storageLinks = [];

  var url = tabs[0].url;
  var title = tabs[0].title;
  var urlSaved = false;


  listLinks();

  // Delete handler
  $('body').on('click', '.delete', function(){ 
    selected_index = parseInt($(this).data("id").replace("delete", ""));
    deleteLinks(); 
    listLinks(); 
  }); 

  // Add handler
  $('.saveLink').on('click', function() {
    if ( !urlSaved ) {
      urlSaved = true;
      addLink();
      listLinks();
    } else {
      $(".saveLink span").fadeOut(function() {
        $(this).parent().addClass('error');
        $(this).text("Already saved!").fadeIn();
      });
    }
  });

  // Add
  function addLink() {
    var link = JSON.stringify({
        linkUrl   : url,
        linkTitle : title
      });
      storageLinks.push(link);
      localStorage.setItem("storageLinks", JSON.stringify(storageLinks));
      return true;
  }

  // List
  function listLinks() {
    $('.saved').empty();

    for(var i in storageLinks){
      var lin = JSON.parse(storageLinks[i]);

      var html = '<li><span data-id="delete'+i+'" class="delete"></span><a target="blank" href="'+lin.linkUrl+'">'+lin.linkTitle+'</a></li>';
      $('.saved').append( html );
    }
  }

  function deleteLinks(){ 
    storageLinks.splice(selected_index, 1); 
    localStorage.setItem("storageLinks", JSON.stringify(storageLinks)); 
  }

});